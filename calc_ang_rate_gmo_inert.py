# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 18:53:47 2020

@author: Tim Russell

Calculates the angular rate of the GMO-pointing reference frame with respect 
to an inertial frame as a function of time. 

Input:
    RAAN_LMO - Orbit right ascension of the ascending node for LMO S/C (deg)
    inc_LMO - Orbit inclination for LMO S/C (deg)
    TA0_LMO - Orbit true anomaly at epoch t0 for LMO S/C (deg)
    r_LMO - Orbit radius for LMO S/C (km)
    n_LMO - Orbit mean motion for LMO S/C [rad/s]
    RAAN_GMO - Orbit right ascension of the ascending node for GMO S/C (deg)
    inc_GMO - Orbit inclination for GMO S/C (deg)
    TA0_GMO - Orbit true anomaly at epoch t0 for GMO S/C (deg)
    r_GMO - Orbit radius for GMO S/C (km)
    n_GMO - Orbit mean motion for GMO S/C [rad/s]
    t - Time elapsed since epoch t0 for GMO S/C [s]

Output:
    ang_rate - A vector describing the rotation of the GMO-pointing frame 
               with respect to the inertial frame represented in inertial 
               coordinates [1x3 matrix, rad/s]

"""

def calc_ang_rate_gmo_inert(RAAN_LMO, inc_LMO, TA0_LMO, r_LMO, n_LMO, 
                            RAAN_GMO, inc_GMO, TA0_GMO, r_GMO, n_GMO, t):
    import numpy as np
    from dcm_inert_to_gmo import dcm_inert_to_gmo
    
    # Calculate [MN](t)
    DCM = dcm_inert_to_gmo(RAAN_LMO, inc_LMO, TA0_LMO, r_LMO, n_LMO, RAAN_GMO, 
                     inc_GMO, TA0_GMO, r_GMO, n_GMO, t)
    # Calculate [MN](t-dt)
    DCM1 = dcm_inert_to_gmo(RAAN_LMO, inc_LMO, TA0_LMO, r_LMO, n_LMO, RAAN_GMO, 
                     inc_GMO, TA0_GMO, r_GMO, n_GMO, t-0.001)
    # Calculate [MN](t+dt)
    DCM2 = dcm_inert_to_gmo(RAAN_LMO, inc_LMO, TA0_LMO, r_LMO, n_LMO, RAAN_GMO, 
                     inc_GMO, TA0_GMO, r_GMO, n_GMO, t+0.001)
    # Calculate d[MN]/dt(t)
    DCM_dot = (DCM2-DCM1)/0.002
    
    # Calculate w_tilde = -d[MN]/dt*[MN]'
    omega_tilde = -DCM_dot @ np.transpose(DCM)
    
    # Calculate w in M frame
    ang_rate = np.array([[omega_tilde[2][1]],[omega_tilde[0][2]],
                         [omega_tilde[1][0]]])
    # Calculate w in N frame
    ang_rate = np.transpose(DCM) @ ang_rate
    
    return ang_rate
    
    
        

