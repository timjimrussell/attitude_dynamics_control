# -*- coding: utf-8 -*-
"""
Created on Sat Apr  4 15:25:42 2020

@author: Tim Russell

Calculates the inertial derivative of a the position of a point mass in 
circular orbit. As eccentricity is 0, the radius never changes and the 
in-frame derivative is zero. Thus, only the transport velocity term survives.

Input:
    r - The radius vector of the circular orbit in orbit frame coordinates
        [1x3 vector, km]
    n - The mean motion of the orbit [rad/s]
    
Output:
    rdot - The velocity vector of the point mass in orbit frame coordinates
           [1x3 vector, km/s]
"""

def vel_orbit_frame(r,n):
    import numpy as np
    from cross_matrix import cross_matrix
    
    omega = np.array([[0],[0],[n]])
    omega_tilde = cross_matrix(omega)
    rdot = omega_tilde @ r
    
    return rdot

