# -*- coding: utf-8 -*-
"""
Created on Sat Apr  4 15:48:34 2020

@author: Tim Russell

Finds the inertial position and velocity of a spacecraft in a circular orbit 
for a given time elapsed from an epoch.

Input:
    r - Orbit radius [km]
    n - Orbit mean motion [rad/s]
    RAAN - Orbit right ascension of the ascending node [deg]
    inc - Orbit inclination [deg]
    TA0 - Orbit true anomaly at epoch [deg]
    t - Time since epoch [s]
    
Output:
    Nr - Orbit position in inertial coordinates [3x1 vector, km]
    Nrdot - Orbit velocity in inertial coordinates [3x1 vector, km/s]
"""

def calc_inert_vecs(r, n, RAAN, inc, TA0, t):
    import numpy as np
    from vel_orbit_frame import vel_orbit_frame
    from calc_TA import calc_TA
    from orb_to_inert import orb_to_inert
    
    # Build state vectors in orbit coordinates
    Or = np.array([[r],[0],[0]])
    Ordot = vel_orbit_frame(Or, n)
    
    # Calculate current true anomaly
    TA = calc_TA(TA0, n, t)
    
    # Calculate state vectors in inertial coordinates
    Nr = orb_to_inert(Or, RAAN, inc, TA)
    Nrdot = orb_to_inert(Ordot, RAAN, inc, TA)
    
    return Nr, Nrdot


