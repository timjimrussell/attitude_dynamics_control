# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 23:32:21 2020

@author: Tim Russell

Calculates the angular rate of the nadir-pointing reference frame with respect 
to an inertial frame as a function of time. But again, this is time-invariant 
so whatever.

Input:
    RAAN - Orbit right ascension of the ascending node (deg)
    inc - Orbit inclination (deg)
    TA0 - Orbit true anomaly at epoch t0 (deg)
    n - Orbit mean motion [rad/s]
    t - Time elapsed since epoch t0 [s]
    
Output:
    ang_rate - A vector describing the rotation of the nadir-pointing frame 
               with respect to the inertial frame represented in inertial 
               coordinates [1x3 matrix, rad/s]

"""

def calc_ang_rate_nadir_inert(RAAN, inc, TA0, n, t):
    import numpy as np
    from dcm_inert_to_orb import dcm_inert_to_orb
    
    ang_rate_O = np.array([[0],[0],[n]])
    DCM = np.transpose(dcm_inert_to_orb(RAAN, inc, TA0, n, t))
    
    ang_rate = DCM @ ang_rate_O
    
    return ang_rate

