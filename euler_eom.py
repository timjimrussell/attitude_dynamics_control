# -*- coding: utf-8 -*-
"""
Created on Sun Mar 29 13:46:00 2020

@author: Tim Russell

Uses Euler's equation of motion for rotating bodies to calculate the 
instantaneous attitude rate derivative (rad/s^2) given a body inertia tensor 
(kg-m^2/s), an attitude rate vector (rad/s), and a net external torque vector 
(kg-m^2/s^2) 
"""


def euler_eom(I, omega, L):
    from numpy.linalg import inv
    from cross_matrix import cross_matrix
    
    omega_tilde = cross_matrix(omega)
    
    omega_dot = inv(I)@(-omega_tilde@I@omega + L)
    
    return omega_dot
