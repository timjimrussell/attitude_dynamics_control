# -*- coding: utf-8 -*-
"""
Created on Sun Mar 22 11:38:51 2020

@author: Tim Russell

Converts a 3x1 vector into a skew-symmetric matrix equivalent to the cross 
product operator.
"""

def cross_matrix(vector):
    import numpy as np
    a=vector[0,0]
    b=vector[1,0]
    c=vector[2,0]
    
    matrix = np.array([[0, -c, b],
                      [c, 0, -a],
                      [-b, a, 0]])
    return matrix
