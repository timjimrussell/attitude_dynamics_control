# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 23:25:02 2020

@author: Tim Russell

Calculates the DCM mapping from inertial coordinates to Nadir-pointing 
coordinates at a given time elapsed from epoch t0.

Input:
    RAAN - Orbit right ascension of the ascending node (deg)
    inc - Orbit inclination (deg)
    TA0 - Orbit true anomaly at epoch t0 (deg)
    n - Orbit mean motion [rad/s]
    t - Time elapsed since epoch t0 [s]
    
Output:
    DCM - A matrix mapping a vector in inertial coordinates to one in orbit 
    coordinates [3x3 matrix]
"""

def dcm_inert_to_nadir(RAAN, inc, TA0, n, t):
    import numpy as np
    from dcm_inert_to_orb import dcm_inert_to_orb
    
    DCM_ON = dcm_inert_to_orb(RAAN, inc, TA0, n, t)
    
    DCM_GO = np.array([[-1, 0, 0],
                       [0, 1, 0],
                       [0, 0, -1]])

    DCM = DCM_GO @ DCM_ON
    
    return DCM
    
