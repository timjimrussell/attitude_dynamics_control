# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 22:34:36 2020

@author: Tim Russell

Calculates the DCM mapping from inertial coordinates to the Sun-pointing 
reference frame.

Input:
    t - Time elapsed since epoch t0 [s] but really this is just a trick as the 
        DCM is invariant with time
    
Output:
    DCM - A matrix mapping a vector in inertial coordinates to one in orbit 
        coordinates [3x3 matrix]
    
"""

def dcm_inert_to_sun(t):
    import numpy as np

    DCM = np.array([[-1, 0, 0],
                    [0, 0, 1],
                    [0, 1, 0]])
    
    return DCM
