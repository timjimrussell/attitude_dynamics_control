# -*- coding: utf-8 -*-
"""
Created on Sun Mar 29 11:58:01 2020

@author: Tim Russell

Propagate attitude (MRP) and rates (rad/s) assuming a single rigid body and no 
external torques. Uses an RK4 integrator. Inputs are an initial state vector 
consisting of [rates; MRPs], time over which to propagate (s), time between 
steps (s), and body inertial matrix (kg-m^2/s). Output is collection of 
state vectors at each time step.
"""

def torque_free_motion_integrator_mrp(X0, tspan, tstep, I):
    import numpy as np
    from numpy.linalg import norm
    from mrp_dke import mrp_dke
    from euler_eom import euler_eom
    from mrp_shadow_set import mrp_shadow_set
    import matplotlib.pyplot as plt
    
    steps = int(tspan/tstep)
    
    X = np.zeros((6, steps+1))
    t = np.zeros(steps+1)
    L = np.zeros((3,1))
    
    X[:,0] = X0.T
    t[0] = 0
    
    for i in range(0, steps):        
        # Current state
        omega = np.array([[X[0,i]],[X[1,i]],[X[2,i]]])
        sigma = np.array([[X[3,i]],[X[4,i]],[X[5,i]]])
        
        # Uncomment the following if you want to ensure angular momentum and 
        # kinetic energy preserved at each step
        # H = norm(I@omega)
        # T = 0.5*(I[0,0]*omega[0,0]**2 + I[1,1]*omega[1,0]**2 + 
        #           I[2,2]*omega[2,0]**2)
        # print(f'H = {H}, T = {T}\n')
        
        # k1
        omega1 = omega
        omega_dot1 = euler_eom(I, omega1, L)
        k1_omega = tstep*omega_dot1
        sigma1 = sigma
        sigma_dot1 = mrp_dke(sigma1, omega1)
        k1_sigma = tstep*sigma_dot1
        
        # k2
        omega2 = omega + 0.5*k1_omega
        omega_dot2 = euler_eom(I, omega2, L)
        k2_omega = tstep*omega_dot2
        sigma2 = sigma + 0.5*k1_sigma
        sigma_dot2 = mrp_dke(sigma2, omega2)
        k2_sigma = tstep*sigma_dot2
        
        # k3
        omega3 = omega + 0.5*k2_omega
        omega_dot3 = euler_eom(I, omega3, L)
        k3_omega = tstep*omega_dot3
        sigma3 = sigma + 0.5*k2_sigma
        sigma_dot3 = mrp_dke(sigma3, omega3)
        k3_sigma = tstep*sigma_dot3

        # k4
        omega4 = omega + k3_omega
        omega_dot4 = euler_eom(I, omega4, L)
        k4_omega = tstep*omega_dot4
        sigma4 = sigma + k3_sigma
        sigma_dot4 = mrp_dke(sigma4, omega4)
        k4_sigma = tstep*sigma_dot4
        
        # Next state
        omega_next = omega + 1/6*(k1_omega + 2*k2_omega + 
                                      2*k3_omega + k4_omega)
        sigma_next = sigma + 1/6*(k1_sigma + 2*k2_sigma + 
                                      2*k3_sigma + k4_sigma)
        
        # Shadow set conversion if necessary
        if norm(sigma_next) > 1:
            sigma_next = mrp_shadow_set(sigma_next)
        
        X[:,i+1] = np.append(omega_next, sigma_next).T  
        t[i+1] = tstep*(i+1)

    plt.figure(num=None, figsize=(12, 8), dpi=300, facecolor='w', edgecolor='k')
    
    plt.subplot(2, 3, 1)
    plt.plot(t, X[0,:])
    plt.title('$\omega_1$')
    plt.ylabel('$rad/s$')
    
    plt.subplot(2, 3, 2)
    plt.plot(t, X[1,:])
    plt.title('$\omega_2$')
    
    plt.subplot(2, 3, 3)
    plt.plot(t, X[2,:])
    plt.title('$\omega_3$')
    
    plt.subplot(2, 3, 4)
    plt.plot(t, X[3,:])
    plt.title('$\sigma_1$')
    plt.xlabel('$s$')
    
    plt.subplot(2, 3, 5)
    plt.plot(t, X[4,:])
    plt.title('$\sigma_2$')
    plt.xlabel('$s$')
    
    plt.subplot(2, 3, 6)
    plt.plot(t, X[5,:])
    plt.title('$\sigma_3$')
    plt.xlabel('$s$')
    
    return X
        
        
        
        
