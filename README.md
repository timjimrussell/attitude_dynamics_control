# attitude_kinematics

Describe and propagate attitude using DCM, Euler angle, primary rotation vector, quaternion, CRP, and MRP representations.