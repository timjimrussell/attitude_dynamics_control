# -*- coding: utf-8 -*-
"""
Created on Sat Apr  4 16:57:16 2020

@author: Tim Russell

Calculates the DCM mapping from inertial coordinates to orbit coordinates at a
given time elapsed from epoch t0.

Input:
    RAAN - Orbit right ascension of the ascending node (deg)
    inc - Orbit inclination (deg)
    TA0 - Orbit true anomaly at epoch t0 (deg)
    n - Orbit mean motion [rad/s]
    t - Time elapsed since epoch t0 [s]
    
Output:
    DCM - A matrix mapping a vector in inertial coordinates to one in orbit 
          coordinates [3x3 matrix]
"""

def dcm_inert_to_orb(RAAN, inc, TA0, n, t):
    import numpy as np
    import math
    from calc_TA import calc_TA
    
    TA = calc_TA(TA0, n, t)
    
    c1 = math.cos(RAAN * math.pi/180)
    s1 = math.sin(RAAN * math.pi/180)
    c2 = math.cos(inc * math.pi/180)
    s2 = math.sin(inc * math.pi/180)
    c3 = math.cos(TA * math.pi/180)
    s3 = math.sin(TA * math.pi/180)
    
    DCM = np.array([[c3*c1-s3*c2*s1, c3*s1+s3*c2*c1, s3*s2],
                    [-s3*c1-c3*c2*s1, -s3*s1+c3*c2*c1, c3*s2],
                    [s2*s1, -s2*c1, c2]])
    
    return DCM

