# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 20:16:13 2020

@author: Tim Russell

Calculates tracking error for a body frame with respect to a reference frame.

Input:
    sigma_BN - MRP description of the body frame with respect to the inertial 
               frame [3x1 vector]
    omega_BN
    DCM_RN - The DCM mapping inertial coordinates to reference frame 
             coordinates [3x3 matrix]
    omega_RN

Output:
    sigma_BR - MRP description of the body frame with respect to the reference
               frame [3x1 vector]
    omega_BR
            
"""

def mrp_inert_to_ref(sigma_BN, omega_BN, DCM_RN, omega_RN):
    import numpy as np
    from numpy.linalg import norm
    import math
    from cross_matrix import cross_matrix
    
    sigma_BN_tilde = cross_matrix(sigma_BN)
    
    DCM_BN = np.identity(3) + (8*sigma_BN_tilde@sigma_BN_tilde - \
                               4*(1-norm(sigma_BN)**2)*sigma_BN_tilde) / \
                              (1+norm(sigma_BN)**2)**2
                              
    DCM_BR = DCM_BN @ np.transpose(DCM_RN)

    xi = math.sqrt(np.trace(DCM_BR)+1)
    
    sigma_BR = 1/(xi*(xi+2))*np.array([[DCM_BR[1][2]-DCM_BR[2][1]],
                                       [DCM_BR[2][0]-DCM_BR[0][2]],
                                       [DCM_BR[0][1]-DCM_BR[1][0]]])
    
    omega_RN = DCM_BN @ omega_RN
    omega_BR = omega_BN - omega_RN  
    
    return sigma_BR, omega_BR