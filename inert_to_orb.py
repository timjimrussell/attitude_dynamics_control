# -*- coding: utf-8 -*-
"""
Created on Sat Apr  4 16:55:24 2020

@author: Tim Russell

Converts a vector in inertial coordinates to orbit coordinates.

Input:
    Ov - A generic vector in inertial frame coordinates [1x3 vector]
    RAAN - Orbit right ascension of the ascending node (deg)
    inc - Orbit inclination (deg)
    TA - Orbit true anomaly (deg)
    
Output:
    Nv - A generic vector in orbit frame coordinates [1x3 vector]
"""

def inert_to_orb(Ov, RAAN, inc, TA):
    import numpy as np
    import math
    
    c1 = math.cos(RAAN * math.pi/180)
    s1 = math.sin(RAAN * math.pi/180)
    c2 = math.cos(inc * math.pi/180)
    s2 = math.sin(inc * math.pi/180)
    c3 = math.cos(TA * math.pi/180)
    s3 = math.sin(TA * math.pi/180)
    
    DCM = np.array([[c3*c1-s3*c2*s1, c3*s1+s3*c2*c1, s3*s2],
                    [-s3*c1-c3*c2*s1, -s3*s1+c3*c2*c1, c3*s2],
                    [s2*s1, -s2*c1, c2]])
    
    Nv = DCM @ Ov
    
    return Nv