# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 18:14:28 2020

@author: Tim Russell

Calculates the DCM mapping from inertial coordinates to the GMO-pointing 
reference frame.

Input:
    RAAN_LMO - Orbit right ascension of the ascending node for LMO S/C (deg)
    inc_LMO - Orbit inclination for LMO S/C (deg)
    TA0_LMO - Orbit true anomaly at epoch t0 for LMO S/C (deg)
    r_LMO - Orbit radius for LMO S/C (km)
    n_LMO - Orbit mean motion for LMO S/C [rad/s]
    RAAN_GMO - Orbit right ascension of the ascending node for GMO S/C (deg)
    inc_GMO - Orbit inclination for GMO S/C (deg)
    TA0_GMO - Orbit true anomaly at epoch t0 for GMO S/C (deg)
    r_GMO - Orbit radius for GMO S/C (km)
    n_GMO - Orbit mean motion for GMO S/C [rad/s]
    t - Time elapsed since epoch t0 for GMO S/C [s]

Output:
    DCM - A matrix mapping a vector in inertial coordinates to one in orbit 
        coordinates [3x3 matrix]
        
"""

def dcm_inert_to_gmo(RAAN_LMO, inc_LMO, TA0_LMO, r_LMO, n_LMO, RAAN_GMO, 
                     inc_GMO, TA0_GMO, r_GMO, n_GMO, t):
    import numpy as np
    from numpy.linalg import norm
    from calc_TA import calc_TA
    from orb_to_inert import orb_to_inert
    from cross_matrix import cross_matrix
    
    r_LMO_orb = np.array([[r_LMO],[0],[0]])
    TA_LMO = calc_TA(TA0_LMO, n_LMO, t)
    r_LMO_inert = orb_to_inert(r_LMO_orb, RAAN_LMO, inc_LMO, TA_LMO)
    
    r_GMO_orb = np.array([[r_GMO],[0],[0]])
    TA_GMO = calc_TA(TA0_GMO, n_GMO, t)
    r_GMO_inert = orb_to_inert(r_GMO_orb, RAAN_GMO, inc_GMO, TA_GMO)
    
    del_r_inert = r_GMO_inert - r_LMO_inert
    
    m1 = -del_r_inert / norm(del_r_inert)
    
    m2 = cross_matrix(del_r_inert) @ np.array([[0],[0],[1]])
    m2 = m2 / norm(m2)
    
    m3 = cross_matrix(m1) @ m2
    m3 = m3 / norm(m3)
    
    DCM = np.transpose(m1)
    DCM = np.append(DCM, np.transpose(m2), axis=0)
    DCM = np.append(DCM, np.transpose(m3), axis=0)
    
    return DCM
    
